drop table if exists product;


-- создание таблицы
create table product
(
    id         bigserial primary key, -- идентификатор строки - всегда уникальный
    name varchar(20),
    price  float4,
    weight integer,
    color varchar(20),
    quantity integer
);
