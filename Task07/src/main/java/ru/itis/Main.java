package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.hikari.HikariConfiguration;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.repositories.ProductsRepositoryNamedParameterJdbcTemplateImpl;

import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        HikariConfiguration configuration = new HikariConfiguration("/db.properties");

        HikariDataSource dataSource = new HikariDataSource(configuration.getConfig());

        ProductsRepository studentsRepository = new ProductsRepositoryNamedParameterJdbcTemplateImpl(dataSource);
        System.out.println(studentsRepository.findAllByPriceMoreThenOrderByWeightDesc(900));
        Product product = new Product();
        product.setName("Блок питания");
        product.setColor("черный");
        studentsRepository.save(product);

//        ExecutorService executorService = Executors.newFixedThreadPool(50);
//
//        for (int i = 0; i < 10_000; i++) {
//            executorService.submit(() -> {
//              try {
//                  studentsRepository.save(Student.builder()
//                          .firstName("Marsel")
//                          .lastName("Sidikov")
//                          .build());
//              } catch (Exception e) {
//                  e.printStackTrace();
//                  System.exit(0);
//              }
//            });
//        }

    }
}
