package ru.itis.hikari;

import com.zaxxer.hikari.HikariConfig;
import ru.itis.Main;

import java.io.IOException;
import java.util.Properties;

public class HikariConfiguration {
    public HikariConfig getConfig() {
        return config;
    }

    HikariConfig config = new HikariConfig();
    public HikariConfiguration(String propertiesFileName) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream(propertiesFileName));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(20);
    }

}
