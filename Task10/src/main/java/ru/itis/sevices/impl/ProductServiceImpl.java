package ru.itis.sevices.impl;

import org.springframework.dao.DataAccessException;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.sevices.ProductService;

import java.sql.SQLException;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private final ProductsRepository productsRepository;

    public ProductServiceImpl (ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }
    @Override
    public boolean insertProduct(String name, float price, int weight, String color, int quantity) {
        try {
            productsRepository.save(Product.builder()
                    .name(name)
                    .price(price)
                    .color(color)
                    .weight(weight)
                    .quantity(quantity).build());
            return true;
        } catch (DataAccessException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean deleteProductById(Long id) {
        try {
            productsRepository.delete(id);
            return true;
        } catch (DataAccessException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

    @Override
    public List<Product> getProductsList() throws DataAccessException{
        return productsRepository.findAll();
    }
}
