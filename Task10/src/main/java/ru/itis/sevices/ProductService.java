package ru.itis.sevices;

import ru.itis.models.Product;

import java.util.List;

public interface ProductService {
    boolean insertProduct(String name, float price, int weight, String color, int quantity);
    boolean deleteProductById(Long id);
    List<Product> getProductsList();
}
