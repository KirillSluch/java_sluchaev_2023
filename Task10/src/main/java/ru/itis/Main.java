package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.repositories.ProductsRepositoryNamedParameterJdbcTemplateImpl;
import ru.itis.ui.UI;

import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("context.xml");

        applicationContext.getBean(UI.class).start();

    }
}
