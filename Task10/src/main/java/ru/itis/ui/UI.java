package ru.itis.ui;

import org.springframework.dao.DataAccessException;
import ru.itis.models.Product;
import ru.itis.sevices.ProductService;

import java.util.List;
import java.util.Scanner;

public class UI {
    private final Scanner sc = new Scanner(System.in);
    private final ProductService productService;

    public UI(ProductService productService) {
        this.productService = productService;
    }
    public void start() {

        while (true) {
            printInfo();

            String command = sc.nextLine();
            switch (command) {
                case "1" -> {
                    System.out.println("Формат ввода пользователя:\n name\n price\n weight\n color\n quantity");
                    String name = sc.nextLine();
                    float price = Float.parseFloat(sc.nextLine());
                    int weight = Integer.parseInt(sc.nextLine());
                    String color = sc.nextLine();
                    int quantity = Integer.parseInt(sc.nextLine());
                    if (productService.insertProduct(name, price, weight, color, quantity)) {
                        System.out.println("Продукт добавлен");
                    } else {
                        System.out.println("Проблемы с добавлением продукта");
                    }
                }

                case "2" -> {
                    try {
                        List<Product> products = productService.getProductsList();
                        for (Product product : products) {
                            System.out.println(product);
                        }
                    } catch (DataAccessException e) {
                        System.out.println("Не удалось вывести продукты");
                    }
                }
                case "3" -> {
                    Long id = Long.parseLong(sc.nextLine());
                    if (productService.deleteProductById(id)) {
                        System.out.println("Продукт удален");
                    } else {
                        System.out.println("Не удалось удалить продукт");
                    }
                }
                case "4" -> System.exit(0);
                default -> System.out.println("Командк не распознана");
            }
        }
    }
    private void printInfo() {
        System.out.println("Выберите команду:");
        System.out.println("1. Добавить продукт");
        System.out.println("2. Посмотреть список товаров");
        System.out.println("3. Удалить товар по id");
        System.out.println("4. Выход");

    }
}
