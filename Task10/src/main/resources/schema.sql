drop table  if exists product;

CREATE table product (
  id bigserial primary key,
  name varchar(20),
  price float4,
  weight int,
  color varchar(15),
  quantity int
);