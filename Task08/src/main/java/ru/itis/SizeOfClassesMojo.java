package ru.itis;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Mojo(name = "size-of-classes", defaultPhase = LifecyclePhase.COMPILE)
public class SizeOfClassesMojo extends AbstractMojo {
    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFolderFileName;

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String sourceFolderFileName;

    @Parameter(name = "sizeOfClassesFileName", required = true)
    private String sizeOfClassesFileName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File outputFolder = new File(outputFolderFileName);

        File sizeOfClassesFile = new File(outputFolder, sizeOfClassesFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(sizeOfClassesFileName))) {
            getLog().info("Output file for class sizes is in " + sizeOfClassesFileName);

            Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            writer.write(file.getFileName().toString() + " " + Files.size(file));
                            writer.newLine();
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                    });
            getLog().info("Finish work!");
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }

    }
}