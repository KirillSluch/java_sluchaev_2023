package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        usersService.signUp(new SignUpForm("Кирилл2", "Случаев",
                "skiryxa@mail.ru", "123йцук"));
        usersService.signUp(new SignUpForm("Кирилл2", "Случаев",
                "skiryxa1@mail.ru", "123йцук"));
        usersService.signUp(new SignUpForm("Андрей", "Бандеров",
                "andrew@mail.ru", "op[]456"));

        List<User> users = usersRepository.findAll();
        System.out.println(users);

        System.out.println(usersRepository.findById("0853edd5-570d-495c-bbde-fad11e5d5e83"));

        usersRepository.deleteById("5f66cba0-d971-4353-8c0e-74cf77eba458");
        usersRepository.delete(new User(UUID.randomUUID().toString(),"Кирилл2", "Случаев",
                "skiryxa1@mail.ru", "123йцук"));
        usersRepository.update(new User("0853edd5-570d-495c-bbde-fad11e5d5e83", "Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty0010"));



        int i = 0;
    }
}
