package ru.itis.taxi.dto;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class SignUpForm {
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    public SignUpForm(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
