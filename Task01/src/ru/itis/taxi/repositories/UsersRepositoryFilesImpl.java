package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.util.*;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();
    private static User stringToUser(String string) {
        String[] arrayOfFields = string.split("\\|");
        return new User(arrayOfFields[0], arrayOfFields[1], arrayOfFields[2], arrayOfFields[3], arrayOfFields[4]);
    }

    private void rewriteFile(List<User> users) throws FileNotFoundException{
        PrintWriter printWriter = new PrintWriter(fileName);
        printWriter.print("");
        printWriter.close();

        for (User user: users) {
            save(user);
        }
    }

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            String stringFromFile = bufferedReader.readLine();

            while (stringFromFile != null) {
                User user = stringToUser(stringFromFile);
                users.add(user);
                stringFromFile = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID().toString());
        }
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            deleteById(entity.getId());
            save(entity);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User entity) {
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            List<User> users = new ArrayList<>();
            String stringFromFile = bufferedReader.readLine();

            while (stringFromFile != null) {
                User user = stringToUser(stringFromFile);
                if (!(user.equals(entity))) {
                    users.add(user);
                }
                stringFromFile = bufferedReader.readLine();
            }

            rewriteFile(users);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(String id) {
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            List<User> users = new ArrayList<>();
            String stringFromFile = bufferedReader.readLine();

            while (stringFromFile != null) {
                User user = stringToUser(stringFromFile);
                if (!id.equals(user.getId())) {
                    users.add(user);
                }
                stringFromFile = bufferedReader.readLine();
            }

            rewriteFile(users);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(String id) {
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            String stringFromFile = bufferedReader.readLine();

            while (stringFromFile != null) {
                User user = stringToUser(stringFromFile);
                if (id.equals(user.getId())) {
                    return user;
                }
                stringFromFile = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }

    @Override
    public Set<String> getEmails() {
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            Set<String> setOfEmails = new TreeSet<>();
            String stringFromFile = bufferedReader.readLine();

            while (stringFromFile != null) {
                setOfEmails.add(stringFromFile.split("\\|")[3]);
                stringFromFile = bufferedReader.readLine();
            }
            return setOfEmails;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
