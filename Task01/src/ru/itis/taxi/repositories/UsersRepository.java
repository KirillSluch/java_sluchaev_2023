package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.util.UUID;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersRepository extends CrudRepository<String, User> {
}
