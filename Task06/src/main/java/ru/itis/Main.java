package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcTemplateImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Hi!")
                .lastName("Bye")
                .password("qwerty0")
                .build();

        System.out.println(student);
        studentsRepository.save(student);
        studentsRepository.update(Student.builder().id(1L).firstName("Кирилл").lastName("Случаев").age(19).build());
        System.out.println(student);
        studentsRepository.delete(5L);
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(22));
        System.out.println(studentsRepository.findAll());

    }
}
