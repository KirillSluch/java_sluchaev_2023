import entity.User;
import sqlGeneration.SqlGenerator;

public class Main {
    public static void main(String[] args) {
        SqlGenerator sqlGenerator = new SqlGenerator();
        User user = new User();
        user.setFirstName("Kirill");
        user.setLastName("Sluchaev");
        user.setWorker(false);
        System.out.println(sqlGenerator.insert(user));
        System.out.println(sqlGenerator.createTable(User.class));
    }
}
