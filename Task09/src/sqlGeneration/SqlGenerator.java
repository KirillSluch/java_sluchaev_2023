package sqlGeneration;

import java.lang.reflect.Field;
import java.sql.Statement;

public class SqlGenerator {
    public <T> String createTable(Class<T> entityClass) {
        //language=SQL
        String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS ";
        if (entityClass.getAnnotation(TableName.class).value().contains(" ")
                || entityClass.getAnnotation(TableName.class).schema().contains(" ")) {
            throw new RuntimeException("Uncorrected table name!!");
        }

        if (entityClass.getAnnotation(TableName.class) != null) {
            SQL_CREATE_TABLE = SQL_CREATE_TABLE +
                    entityClass.getAnnotation(TableName.class).schema() + "." +
                    entityClass.getAnnotation(TableName.class).value() + " (\n";
        } else {
            throw new RuntimeException("Class has not got sqlGeneration.TableName Annotation!!!");
        }

        Field[] fields = entityClass.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (field.getAnnotation(ColumnName.class) != null) {
                SQL_CREATE_TABLE = SQL_CREATE_TABLE + createTableFormat(field);
                if (i < fields.length - 1) {
                    SQL_CREATE_TABLE = SQL_CREATE_TABLE + ",";
                } else {
                    SQL_CREATE_TABLE = SQL_CREATE_TABLE + ");";
                }
            }

        }
        return SQL_CREATE_TABLE;
    }

    private String createTableFormat(Field field) {
        String SQL_CREATE_TABLE = "";
        SQL_CREATE_TABLE = SQL_CREATE_TABLE + field.getAnnotation(ColumnName.class).value() + " ";
        if (!field.getAnnotation(ColumnName.class).identity()
                && !field.getAnnotation(ColumnName.class).primary()) {
            if (int.class.equals(field.getType())) {
                SQL_CREATE_TABLE = SQL_CREATE_TABLE + "int";
            } else if (String.class.equals(field.getType())) {
                SQL_CREATE_TABLE = SQL_CREATE_TABLE +
                        "varchar(" + field.getAnnotation(ColumnName.class).maxLength() + ")";
            } else if (boolean.class.equals(field.getType())) {
                SQL_CREATE_TABLE = SQL_CREATE_TABLE +
                        "boolean default " + field.getAnnotation(ColumnName.class).defaultBoolean();
            } else if (float.class.equals(field.getType())) {
                SQL_CREATE_TABLE = SQL_CREATE_TABLE + "real";
            } else if (double.class.equals(field.getType())) {
                SQL_CREATE_TABLE = SQL_CREATE_TABLE + "float8";
            }
        } else {
            if (field.getAnnotation(ColumnName.class).identity()) {
                SQL_CREATE_TABLE = SQL_CREATE_TABLE + "bigserial";
            }
            if (field.getAnnotation(ColumnName.class).primary()) {
                SQL_CREATE_TABLE = SQL_CREATE_TABLE + " primary key";
            }
        }
        return SQL_CREATE_TABLE.toString();
    }

    public String insert(Object entity) {
        Class<?> entityClass = entity.getClass();
        if (entityClass.getAnnotation(TableName.class).value().contains(" ")
                || entityClass.getAnnotation(TableName.class).schema().contains(" ")) {
            throw new RuntimeException("Uncorrected table name!!");
        }
        //language=SQL
        String SQL_INSERT = "INSERT INTO " + entityClass.getAnnotation(TableName.class).schema()
                + "." + entityClass.getAnnotation(TableName.class).value();
        Field[] fields = entityClass.getDeclaredFields();

        String insertInto = "(";
        String values = "(";
        try {
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                if (field.getAnnotation(ColumnName.class) != null) {
                    field.setAccessible(true);
                    if (field.get(entity) != null) {
                        insertInto = insertInto + field.getAnnotation(ColumnName.class).value();
                        if (field.get(entity).getClass().equals(String.class)) {
                            values = values + "'" + field.get(entity) + "'";
                        } else {
                            values = values + field.get(entity);
                        }
                        if (i < fields.length - 1) {
                            insertInto = insertInto + ",";
                            values = values + ",";
                        }
                    }
                    field.setAccessible(false);
                }
            }
            insertInto = insertInto + ")";
            values = values + ")";


            SQL_INSERT = SQL_INSERT + insertInto + " VALUES " + values + ";";
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
        return SQL_INSERT;
    }
}
