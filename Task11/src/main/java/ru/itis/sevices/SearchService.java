package ru.itis.sevices;

import ru.itis.models.Product;

import java.util.List;

public interface SearchService {
    List<Product> searchProducts(String query);
}
