package ru.itis.sevices.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.sevices.SearchService;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {

    private final ProductsRepository productsRepository;

    @Override
    public List<Product> searchProducts(String query) {
        if (query == null || query.equals("")) {
            return Collections.emptyList();
        }
        return productsRepository.findAllNameLike(query);

    }
}
