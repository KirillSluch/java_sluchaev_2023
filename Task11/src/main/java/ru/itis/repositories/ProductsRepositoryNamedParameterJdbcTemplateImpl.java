package ru.itis.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.Product;

import javax.sql.DataSource;
import java.util.*;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Repository
public class ProductsRepositoryNamedParameterJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from product where id = :id";

    //language=SQL
    private static final String SQL_SELECT_BY_PRICE_MORE_THAN_ORDER_BY_WEIGHT_DESC =
            "select * from product where price > :minPrice order by weight desc";

    //language=SQL
    private static final String SQL_UPDATE_USER_BY_ID = "update product " +
            "set name = :name, price = :price, weight = :weight, color = :color, quantity = :quantity " +
            "where id = :id";

    //language=SQL
    private static final String SQL_LIKE_BY_NAME = "select * from product where " +
            "(product.name ilike :query)";

    //language=SQL
    private static final String SQL_SELECT_ALL_SORTED_BY = "select * from product order by ";

    //language=SQL
    private static final String SQL_DELETE = "delete from product where id = :id";

    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
        return namedParameterJdbcTemplate;
    }

    private static final RowMapper<Product> productMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .price(row.getObject("price", Float.class))
            .weight(row.getObject("weight", Integer.class))
            .color(row.getString("color"))
            .quantity(row.getObject("quantity", Integer.class))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductsRepositoryNamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public List<Product> findAllNameLike(String query) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("query", "%" + query + "%");

        return namedParameterJdbcTemplate.query(SQL_LIKE_BY_NAME, paramsAsMap, productMapper);
    }

    @Override
    public void save(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("name", product.getName());
        paramsAsMap.put("price", product.getPrice());
        paramsAsMap.put("weight", product.getWeight());
        paramsAsMap.put("color", product.getColor());
        paramsAsMap.put("quantity", product.getQuantity());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("product")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();

        product.setId(id);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    productMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void updateById(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("id", product.getId());
        paramsAsMap.put("name", product.getName());
        paramsAsMap.put("price", product.getPrice());
        paramsAsMap.put("weight", product.getWeight());
        paramsAsMap.put("color", product.getColor());
        paramsAsMap.put("quantity", product.getQuantity());

        namedParameterJdbcTemplate.update(SQL_UPDATE_USER_BY_ID, paramsAsMap);
    }

    @Override
    public void delete(Long id) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("id", id);
        namedParameterJdbcTemplate.update(SQL_DELETE, paramsAsMap);
    }

    @Override
    public List<Product> findAllByPriceMoreThenOrderByWeightDesc(int minPrice) {
        Map<String, Object> params = new HashMap<>();

        params.put("minPrice", minPrice);

        return namedParameterJdbcTemplate.query(SQL_SELECT_BY_PRICE_MORE_THAN_ORDER_BY_WEIGHT_DESC, params, productMapper);
    }

    @Override
    public List<Product> findAllOrderBy(String columnName, boolean order) {
        String SQL_SELECT = SQL_SELECT_ALL_PRODUCTS;
        if (columnName.equals("name")) {
            SQL_SELECT = SQL_SELECT_ALL_SORTED_BY + "name";
        } else if (columnName.equals("color")) {
            SQL_SELECT = SQL_SELECT_ALL_SORTED_BY + "color";
        } else if (columnName.equals("weight")) {
            SQL_SELECT = SQL_SELECT_ALL_SORTED_BY + "weight";
        } else if (columnName.equals("price")) {
            SQL_SELECT = SQL_SELECT_ALL_SORTED_BY + "price";
        } else if (columnName.equals("quantity")) {
            SQL_SELECT = SQL_SELECT_ALL_SORTED_BY + "quantity";
        }

        if (!SQL_SELECT.equals(SQL_SELECT_ALL_PRODUCTS) && !order) {
            SQL_SELECT = SQL_SELECT + " desc";
        }

        return namedParameterJdbcTemplate.query(SQL_SELECT, productMapper);
    }


}
