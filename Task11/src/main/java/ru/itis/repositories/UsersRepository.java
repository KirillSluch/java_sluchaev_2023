package ru.itis.repositories;

import ru.itis.models.User;

import java.util.List;
import java.util.Optional;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersRepository {
    List<User> findAll();

    void save(User user);

    Optional<User> findById(Long id);

    void update(User user);

    void delete(Long id);

    List<User> findAllByAgeInRangeOrderByIdDesc(int minAge, int maxAge);

    List<User> findAllByFirstNameOrLastNameLike(String query);

    Optional<User> findOneByEmail(String email);
}
