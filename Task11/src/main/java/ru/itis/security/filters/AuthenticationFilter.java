package ru.itis.security.filters;


import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.

        if (request.getRequestURI().equals(request.getContextPath() + "/signIn.html")) {
            filterChain.doFilter(request, response);
            return;
        }

        HttpSession session = request.getSession(false);

        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");
            if (authenticated != null && authenticated) {
                filterChain.doFilter(request, response);
                return;
            }
        } else {
            setCookiePath(request, response);
        }
        response.sendRedirect(request.getContextPath() + "/signIn.html");
    }
    private void setCookiePath(HttpServletRequest request, HttpServletResponse response) {
        Cookie wantedURI = new Cookie("wantedURI", request.getRequestURI());
        response.addCookie(wantedURI);
    }


}
