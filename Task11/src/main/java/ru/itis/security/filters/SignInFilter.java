package ru.itis.security.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import ru.itis.security.AuthenticationManager;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;


@WebFilter("/signIn")
public class SignInFilter implements Filter {

    private AuthenticationManager authenticationManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("springContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getMethod().equals("POST")) {
            String email = request.getParameter("email");
            String password = request.getParameter("password");

            if (authenticationManager.authenticate(email, password)) {
                HttpSession session = request.getSession(true);
                session.setAttribute("authenticated", true);
                Optional<Cookie> wantedURI = Arrays.stream(request.getCookies())
                        .filter(c -> c.getName().equals("wantedURI"))
                        .findFirst();
                response.addCookie(new Cookie("wantedURI", request.getContextPath() + "/html/profile.html"));
                if (wantedURI.isPresent()) {
                    response.sendRedirect(wantedURI.get().getValue());
                } else {
                    response.sendRedirect(request.getContextPath() + "/html/profile.html");
                }
            } else {
                response.sendRedirect(request.getContextPath() + "/signIn.html" + "?error");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}

