package ru.itis.servlets;


import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.models.Product;
import ru.itis.sevices.SearchService;

import java.io.IOException;
import java.util.List;

@WebServlet("/products/search")
public class SearchServlet extends HttpServlet {
    private SearchService searchService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.searchService = context.getBean(SearchService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("query");
        List<Product> products = searchService.searchProducts(query);
        System.out.println(products);
        String jsonResponse = objectMapper.writeValueAsString(products);
        response.setContentType("application/json");
        response.getWriter().write(jsonResponse);

    }
}
