package ru.itis.servlets.listeners;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.config.ApplicationConfig;

@WebListener
public class SpringConfigServletContextListener implements ServletContextListener {

    private HikariDataSource dataSource;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.dataSource = context.getBean(HikariDataSource.class);
        event.getServletContext().setAttribute("springContext", context);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        this.dataSource.close();
    }
}
