package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/Task11_war_exploded/html/profile.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("columnName") != null) {
            String sorting = req.getParameter("columnName");
            Cookie cookie = new Cookie("columnName", sorting);
            resp.addCookie(cookie);
            resp.sendRedirect("/Task11_war_exploded/profile");
        } else if (req.getParameter("desc") != null) {
            String sorting = req.getParameter("desc");
            Cookie cookie = new Cookie("desc", sorting);
            resp.addCookie(cookie);
            resp.sendRedirect("/Task11_war_exploded/profile");
        }
    }
}
