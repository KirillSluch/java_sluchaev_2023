function searchProducts(query) {
    return fetch('/Task11_war_exploded/products/search?query=' + query)
        .then((response) => {
            return response.json();
        })
        .then((products) => {
            fillTable(products);
            console.log(products)
        })
}

function fillTable(products) {
    let table = document.getElementById("usersTable");

    table.innerHTML = '<tr>\n' +
        '            <th>id</th>\n' +
        '            <th>Name</th>\n' +
        '            <th>Price</th>\n' +
        '            <th>Weight</th>\n' +
        '            <th>Color</th>\n' +
        '            <th>Quantity</th>\n' +
        '        </tr>';

    for (let i = 0; i < products.length; i++) {
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let nameCell = row.insertCell(1);
        let priceCell = row.insertCell(2);
        let weightCell = row.insertCell(2);
        let colorCell = row.insertCell(2);
        let quantityCell = row.insertCell(2);

        idCell.innerHTML = products[i].id;
        nameCell.innerHTML = products[i].name;
        priceCell.innerHTML = products[i].price;
        weightCell.innerHTML = products[i].weight;
        colorCell.innerHTML = products[i].color;
        quantityCell.innerHTML = products[i].quantity;
    }

}

function addProduct(name, price, weight, color, quantity) {
    let body = {
        "name": name,
        "price": price,
        "weight": weight,
        "color": color,
        "quantity": quantity
    }

    fetch('/Task11_war_exploded/add', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(r => window.location.replace(r.url))
}