<%@ page import="ru.itis.models.User" %><%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 05.12.2022
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
 <meta charset="UTF-8">
 <title>Profile</title>
</head>
<body>
<form action="/Task11_war_exploded/profile" method="post">
 <input type="radio" id="name" name="columnName" value="name">
 <label for="name">Name</label>

 <input type="radio" id="price" name="columnName" value="price">
 <label for="price">Price</label>

 <input type="radio" id="color" name="columnName" value="color">
 <label for="color">Color</label>

 <input type="radio" id="quantity" name="columnName" value="quantity">
 <label for="quantity">Quantity</label>

 <input type="radio" id="weight" name="columnName" value="weight">
 <label for="color">Weight</label>
 <button type="submit">Submit</button>
</form>
<form action="/Task11_war_exploded/profile" method="post">
 <input type="radio" id="desc" name="desc" value="true">
 <label for="desc">По убыванию</label>

 <input type="radio" id="simple" name="desc" value="false">
 <label for="simple">По возрастанию</label>

 <button type="submit">Submit</button>
</form>
<form action="/Task11_war_exploded/files/upload" method="post" enctype="multipart/form-data">
 <label>
  Description
  <input type="text" name="description" placeholder="Enter description...">
 </label>
 <input type="file" name="file">
 <input type="submit" value="Upload">
</form>
<% if (session.getAttribute("user") != null) {
   String logo = ((User) session.getAttribute("user")).getLogoName();
   if (logo != null) { %>
    <img src="/Task11_war_exploded/files/<%= logo%>" width="50"
     height="50" border="0" alt="Лого">
   <% } }%>



</body>
</html>
