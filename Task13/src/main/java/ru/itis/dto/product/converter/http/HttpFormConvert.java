package ru.itis.dto.product.converter.http;

import jakarta.servlet.http.HttpServletRequest;
import ru.itis.dto.product.ProductDto;

public class HttpFormConvert {
    public static ProductDto from(HttpServletRequest request) {
        return ProductDto.builder()
                .name(request.getParameter("name"))
                .price(Float.valueOf(request.getParameter("price")))
                .weight(Integer.valueOf(request.getParameter("weight")))
                .color(request.getParameter("password"))
                .quantity(Integer.valueOf(request.getParameter("quantity")))
                .build();
    }

}
