package ru.itis.services;

import ru.itis.dto.product.ProductDto;
import ru.itis.models.Product;

import java.util.List;

public interface ProductService {
    boolean insertProduct(ProductDto data);
    boolean deleteProductById(Long id);
    List<Product> getProductsList();
    List<Product> getProductsListGreaterThisPrice(int minPrice);
    List<Product> getProductsListSorted(String param, boolean desc);
}
