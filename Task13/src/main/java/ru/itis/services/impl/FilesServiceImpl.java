package ru.itis.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.itis.dto.FileDto;
import ru.itis.models.FileInfo;
import ru.itis.models.User;
import ru.itis.repositories.FilesRepository;
import ru.itis.services.FilesService;
import ru.itis.services.UsersService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static ru.itis.dto.FileDto.from;

@Service
public class FilesServiceImpl implements FilesService {

    @Value("${storage.path}")
    private String storagePath;

    private final FilesRepository filesRepository;
    private final UsersService usersService;

    public FilesServiceImpl(FilesRepository filesRepository, UsersService usersService) {
        this.filesRepository = filesRepository;
        this.usersService = usersService;
    }

    @Override
    public void upload(FileDto file, User user) {
        String originalFileName = file.getFileName();
        String extension = originalFileName.substring(originalFileName.lastIndexOf("."));
        String storageFileName = UUID.randomUUID() + extension;

        user.setLogoName(storageFileName);
        usersService.updateLogo(user);
        
        FileInfo fileInfo = FileInfo.builder()
                .description(file.getDescription())
                .size(file.getSize())
                .mimeType(file.getMimeType())
                .originalFileName(originalFileName)
                .storageFileName(storageFileName)
                .build();

        try {
            Files.copy(file.getFileStream(), Paths.get(storagePath + storageFileName));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        filesRepository.save(fileInfo);
    }

    @Override
    public FileDto getFile(String fileName) {
        FileInfo file = filesRepository.findByStorageFileName(fileName).orElseThrow();
        FileDto fileDto = from(file);
        fileDto.setPath(Paths.get(storagePath + "\\" + fileName));
        return fileDto;
    }

    @Override
    public String getFileStorageName(String fileName) {
        FileInfo file = filesRepository.findByStorageFileName(fileName).orElseThrow();

        return file.getStorageFileName();
    }
}

