package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import ru.itis.dto.product.ProductDto;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductsRepository productsRepository;

    @Override
    public boolean insertProduct(ProductDto data) {
        try {
            productsRepository.save(Product.builder()
                    .name(data.getName())
                    .price(data.getPrice())
                    .color(data.getColor())
                    .weight(data.getWeight())
                    .quantity(data.getQuantity()).build());
            return true;
        } catch (DataAccessException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean deleteProductById(Long id) {
        try {
            productsRepository.delete(id);
            return true;
        } catch (DataAccessException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

    @Override
    public List<Product> getProductsList() throws DataAccessException{
        return productsRepository.findAll();
    }

    @Override
    public List<Product> getProductsListGreaterThisPrice(int minPrice) {
        return productsRepository.findAllByPriceMoreThenOrderByWeightDesc(minPrice);
    }

    @Override
    public List<Product> getProductsListSorted(String param, boolean desc) {
        return productsRepository.findAllOrderBy(param, desc);
    }
}
