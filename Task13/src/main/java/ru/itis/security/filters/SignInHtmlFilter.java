package ru.itis.security.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

//@WebFilter("/signIn.html")
public class SignInHtmlFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        if (session != null
                && session.getAttribute("authenticated") != null
                && (boolean)session.getAttribute("authenticated")) {
            response.sendRedirect(request.getContextPath() + "/html/profile.html");
            return;
        }

        chain.doFilter(request, response);
    }
}
