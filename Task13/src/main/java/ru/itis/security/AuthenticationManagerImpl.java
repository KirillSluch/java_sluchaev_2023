package ru.itis.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;

@RequiredArgsConstructor
@Component
public class AuthenticationManagerImpl implements AuthenticationManager {

    private final UsersRepository usersRepository;


    @Override
    public boolean authenticate(String email, String password) {
        User user = usersRepository.findOneByEmail(email).orElse(null);

        if (user == null) {
            return false;
        }

        return user.getPassword().equals(password);
    }

    @Override
    public User getUser(String email, String password) {
        return usersRepository.findOneByEmail(email).orElse(null);
    }
}
