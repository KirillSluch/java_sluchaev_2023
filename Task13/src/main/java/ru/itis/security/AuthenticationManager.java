package ru.itis.security;

import ru.itis.models.User;

public interface AuthenticationManager {
    boolean authenticate(String email, String password);
    User getUser(String email, String password);
}
