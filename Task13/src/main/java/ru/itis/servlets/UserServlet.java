package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.user.SignUpDto;
import ru.itis.dto.user.converters.http.HttpFormsConverter;
import ru.itis.services.UsersService;

import java.io.IOException;

@WebServlet("/signUp")
public class UserServlet extends HttpServlet {
    private UsersService usersService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UsersService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getRequestURI().equals(request.getContextPath() + "/signUp")) {
            SignUpDto signUpData = HttpFormsConverter.from(request);
            usersService.signUp(signUpData);
            response.sendRedirect(request.getContextPath() + "/profile");
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
