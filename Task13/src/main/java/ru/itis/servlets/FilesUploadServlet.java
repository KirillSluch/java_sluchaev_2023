package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.models.User;
import ru.itis.services.FilesService;
import ru.itis.services.UsersService;
import ru.itis.services.impl.FilesServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@WebServlet("/files/upload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {
    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String description = new BufferedReader(
                        new InputStreamReader(
                                request.getPart("description").getInputStream()
                        )).readLine();

        Part part = request.getPart("file");

        FileDto fileDto = FileDto.builder()
                .fileName(part.getSubmittedFileName())
                .mimeType(part.getContentType())
                .size(part.getSize())
                .description(description)
                .fileStream(part.getInputStream())
                .build();

        User user = (User) session.getAttribute("user");
        filesService.upload(fileDto, user);
        response.sendRedirect("/Task11_war_exploded/profile");
    }
}
