package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.product.ProductDto;
import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "AddServlet", value = {"/product", "/add", "/filter"})
public class AddServlet extends HttpServlet {
    private ProductService productService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.productService = context.getBean(ProductService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURI().equals("/Task11_war_exploded/product")){
            List<Product> products;
            String minPrice = request.getParameter("minPrice");
            if (minPrice != null) {
                products = productService.getProductsListGreaterThisPrice(Integer.parseInt(minPrice));
            } else {
                Optional<Cookie> columnName = Arrays.stream(request.getCookies())
                        .filter(c -> c.getName().equals("columnName"))
                        .findFirst();

                Optional<Boolean> desc = Arrays.stream(request.getCookies())
                        .filter(c -> c.getName().equals("desc"))
                        .map(c -> {
                            if (c.getValue().equals("true")) {
                                return true;
                            } else {
                                return false;
                            }
                        })
                        .findFirst();
                if (columnName.isPresent() && desc.isPresent()) {
                    products = productService.getProductsListSorted(columnName.get().getValue(), desc.get());
                } else {
                    products = productService.getProductsList();
                }
            }

            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();

            String html = getHtmlForUsers(products);

            writer.println(html);

        } else if (request.getRequestURI().equals("/Task11_war_exploded/add")) {
            response.sendRedirect("/Task11_war_exploded/html/addProduct.html");
        } else if (request.getRequestURI().equals("/Task11_war_exploded/filter")) {
            response.sendRedirect("/Task11_war_exploded/html/findByParameter.html");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(request.getRequestURI().equals("/Task11_war_exploded/add")) {
            String body = request.getReader().readLine();
            ProductDto data = objectMapper.readValue(body, ProductDto.class);
            productService.insertProduct(data);
            response.sendRedirect("/Task11_war_exploded/product");
        } else {
            response.sendRedirect(request.getContextPath());
        }
    }

    private static String getHtmlForUsers(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Users</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>Name</th>\n" +
                "\t\t<th>Price</th>\n" +
                "\t\t<th>Weight</th>\n" +
                "\t\t<th>Color</th>\n" +
                "\t\t<th>Quantity</th>\n" +
                "\t</tr>");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getName()).append("</td>\n");
            html.append("<td>").append(product.getPrice()).append("</td>\n");
            html.append("<td>").append(product.getWeight()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("<td>").append(product.getQuantity()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }

}
