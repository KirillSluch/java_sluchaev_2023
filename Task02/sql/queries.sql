-- получение данных из таблицы
select first_name, last_name
from driver
order by driving_experience desc, first_name;

select first_name, last_name, number, brand, color, prise
from taxi_order
JOIN client c on c.id = taxi_order.client_id
JOIN car c2 on c2.id = taxi_order.car_id
order by prise desc, taxi_order.id;

select first_name, last_name, start_point, finish_point
from taxi_order
join car c on c.id = taxi_order.car_id
join driver d on d.id = c.driver_id
where prise > 300;
