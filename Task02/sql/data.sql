-- вставка и обновление данных в таблице
insert into client(first_name, last_name)
values ('Кирилл', 'Случаев'),
       ('Андрей', 'Бандеров'),
       ('Ринат', 'Галиев'),
       ('Леша', 'Попович');

insert into driver(first_name, last_name, driving_experience)
values ('Дизель', 'Вин', 20),
       ('Макс', 'Безумный', 10),
       ('Даниель', 'Моралес', 15),
       ('Молния', 'МакКуин ', 14);

insert into car(number, brand, color, driver_id)
values ('А007ТЕ', 'Dodge Charger', 'BLACK', 1),
       ('C017OЕ', 'YAS Patriot', 'GREEN', 2),
       ('K021EO', 'Peugeot 406', 'WHITE', 3),
       ('M016XT', 'Kia Rio', 'RED', 4);

insert into taxi_order(prise, start_point, finish_point, time_of_order, client_id, car_id)
values (200, 'Университет', 'Дом', '12:55:10', 1, 1),
       (150, 'Институт физики', 'Магнит', '17:34:28', 2, 4),
       (459, 'Дом', 'Аэропорт', '20:42:08', 3, 2),
       (354, 'Дом', 'Дом Калывана', '06:59:17', 4, 3);

update driver
set first_name = 'Доминик',
    last_name  = 'Торренто'
where first_name = 'Дизель';

update driver
set order_id = 1
where id = 1;

update driver
set order_id = 2
where id = 4;

update driver
set order_id = 3
where id = 2;

update driver
set order_id = 4
where id = 3;

update driver
set driving_experience = driving_experience + 1
where driving_experience > 15;





