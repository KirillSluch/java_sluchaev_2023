-- создание и редактирование схемы таблицы
drop table if exists client cascade;
drop table if exists driver cascade;
drop table if exists car cascade;
drop table if exists taxi_order cascade;

create table client
(
    id bigserial primary key,
    first_name char(20),
    last_name char(20)
);

create table driver
(
      id bigserial primary key,
      first_name char(20),
      last_name char(20),
      driving_experience integer check ( driving_experience > 5)
);

create table car
(
    id serial primary key,
    number char(10),
    brand char(30),
    color char(15)
);

create table taxi_order
(
    id serial primary key,
    prise integer check ( prise > 0 ),
    start_point char(30),
    finish_point char(30),
    time_of_order time
);

alter table car add driver_id bigint;
alter table car add foreign key(driver_id) references driver(id);

alter table taxi_order add client_id bigint;
alter table taxi_order add foreign key(client_id) references client(id);

alter table taxi_order add car_id bigint;
alter table taxi_order add foreign key(car_id) references car(id);

alter table driver add order_id bigint;
alter table driver add foreign key(order_id) references taxi_order(id);
