package ru.itis.controllers;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class MenuController {
    @GetMapping
    public String getMenu(HttpServletResponse response) {
        response.addCookie(new Cookie("columnName", "name"));
        response.addCookie(new Cookie("desc", "true"));
        return "menu";
    }
}

