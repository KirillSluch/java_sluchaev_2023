package ru.itis.controllers;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;

@Controller
@RequestMapping("/profile")
@RequiredArgsConstructor
public class ProfileController {
    @GetMapping
    public String getProfile(Model model) {
        return "profile";
    }

    @PostMapping
    public String getProfile(@RequestParam("columnName") String columnName,
                             @RequestParam("desc") Boolean desc,
                             HttpServletResponse response) {
        if (columnName != null && desc != null) {
            response.addCookie(new Cookie("columnName", columnName));
            response.addCookie(new Cookie("desc", desc.toString()));
        }

        return "profile";
    }
}
