package ru.itis.controllers;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.product.ProductDto;
import ru.itis.models.Product;
import ru.itis.services.ProductService;
import ru.itis.services.SearchService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final SearchService searchService;
    @GetMapping
    public String showProducts(Model model,
                               @CookieValue("columnName") String columnName,
                               @CookieValue("desc") Boolean desc) {
        model.addAttribute("products", productService.getProductsListSorted(columnName, desc));
        return "products";
    }

    @PostMapping("/add")
    @ResponseBody
    public List<Product> addProduct(@RequestBody ProductDto productDto,
                                    @CookieValue("columnName") String columnName,
                                    @CookieValue("desc") Boolean desc) {
        productService.insertProduct(productDto);
        return productService.getProductsListSorted(columnName, desc);
    }

    @GetMapping("/find")
    public String getFindPage(HttpServletRequest request, Model model) {
        String minPrice = request.getParameter("minPrice");
        if(minPrice == null) {
            model.addAttribute("products", new ArrayList<Product>());
        } else {
            model.addAttribute("products",
                    productService.getProductsListGreaterThisPrice(Integer.parseInt(minPrice)));
        }
        return "find";
    }

    @PostMapping("/find")
    public String findByParam(@RequestParam("minPrice") int minPrice, Model model) {
        model.addAttribute("products", productService.getProductsListGreaterThisPrice(minPrice));

        return "find";
    }

    @GetMapping("/search")
    @ResponseBody
    public List<Product> getSearchPage(@RequestParam("query") String query) {
        return searchService.searchProducts(query);
    }

}
