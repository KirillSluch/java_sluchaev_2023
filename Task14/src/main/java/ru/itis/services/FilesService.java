package ru.itis.services;


import ru.itis.dto.FileDto;
import ru.itis.models.User;

public interface FilesService {
    void upload(FileDto file, User user);

    FileDto getFile(String fileName);

    String getFileStorageName(String fileName);
}

