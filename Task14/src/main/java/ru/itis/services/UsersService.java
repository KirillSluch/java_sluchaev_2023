package ru.itis.services;

import ru.itis.dto.user.SignUpDto;
import ru.itis.dto.user.UserDto;
import ru.itis.models.User;

import java.util.List;

public interface UsersService {
    void signUp(SignUpDto signUpData);
    List<UserDto> getAllUsers();

    List<User> getAllUsersByAge(int ageFrom, int ageTo);

    void updateLogo(User user);
}
