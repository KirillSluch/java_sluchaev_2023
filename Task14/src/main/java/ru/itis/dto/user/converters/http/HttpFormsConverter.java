package ru.itis.dto.user.converters.http;

import jakarta.servlet.http.HttpServletRequest;
import ru.itis.dto.user.SignUpDto;

public class HttpFormsConverter {
    public static SignUpDto from(HttpServletRequest request) {
        return SignUpDto.builder()
                .firstName(request.getParameter("firstName"))
                .lastName(request.getParameter("lastName"))
                .email(request.getParameter("email"))
                .password(request.getParameter("password"))
                .build();
    }
}
