package ru.itis.repositories;

import ru.itis.models.Product;

import java.util.List;
import java.util.Optional;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface ProductsRepository {
    List<Product> findAll();
    List<Product> findAllNameLike(String query);

    void save(Product product);

    Optional<Product> findById(Long id);

    void updateById(Product product);

    void delete(Long id);

    List<Product> findAllByPriceMoreThenOrderByWeightDesc(int minPrice);
    List<Product> findAllOrderBy(String columnName, boolean order);
}
