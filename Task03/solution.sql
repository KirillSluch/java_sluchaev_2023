
-- 1 задача
select model, speed, hd 
from PC 
where price < 500;

-- 2 задача
select distinct(maker) 
from Product 
where type = 'Printer';

-- 3 задача
select model, ram, screen f
rom Laptop 
where price > 1000;

-- 4 задача
select * 
from Printer 
where color = 'y';

-- 5 задача
select model, speed, hd 
from PC 
where (cd = '12x' or cd = '24x') and price < 600;

-- 6 задача
select distinct pt.maker, l.speed
from Product pt
join Laptop l on l.model = pt.model
where l.hd >= 10


--7 задача
select distinct(pc.model), pc.price 
from Product pr join PC pc on pr.model = pc.model 
where maker = 'B' 
union 
select distinct(l.model), l.price from Product pr join Laptop l on pr.model = l.model where maker = 'B'
union
select distinct(p.model), p.price 
from Product pr join Printer p on pr.model = p.model 
where maker = 'B' ;

--8 задача 
select distinct(maker) from Product where type = 'PC'
except
select distinct(maker) from Product where type = 'Laptop';

--9 задача
select distinct(pt.maker) 
from Product pt 
left join PC pc on pt.model = pc.model 
where pc.speed >= 450; 

-- 10 задача
select model, price 
from Printer
where price = (select max(price) from Printer);

-- 11 задача
select avg(speed) 
from PC;

-- 12 задача
select avg(speed) 
from Laptop 
where price > 1000;

-- 13 задача
select avg(speed) 
from PC
where model in 
(select model from Product where maker = 'A');

-- 14 задача 
select cl.class, sh.name, cl.country 
from Ships sh
left join Classes cl on sh.class = cl.class
where cl.numGuns >= 10;

-- 15 задача
select hd 
from PC
group by hd
having count(hd) > 1;

-- 16 задача
select distinct(i.model), j.model, i.speed, i.ram
from PC i, PC j
where i.speed = j.speed and i.ram = j.ram and i.model > j.model;

-- 17 задача
select distinct pt.type, lp.model, lp.speed
from Product pt join Laptop lp on pt.model = lp.model
where lp.speed < all(select speed from PC);

-- 18 задача
select distinct pt.maker, prin.price 
from Product pt
join Printer prin on pt.model = prin.model
where prin.color = 'y' and prin.price = (select min(price) from Printer where color = 'y')

-- 19 задача
select pt.maker, avg(l.screen)
from Product pt
join Laptop l on pt.model = l.model
group by pt.maker;

-- 20 задача
select Maker, count(model) as Model_Count
from Product
where type = 'PC'
group by maker
having count(model) > 2;

--21 задача
select pt.maker, max(pc.price)
from Product pt
right join PC pc on pt.model = pc.model
group by pt.maker;

-- 22 задача
select speed, avg(price)
from PC
where speed > 600
group by speed;

-- 23 задача
select pt.maker
from Product pt
join PC pc on pt.model = pc.model
where pc.speed >= 750
intersect
select distinct pt.maker
from Product pt
join Laptop l on pt.model = l.model
where l.speed >= 750

-- 24 задача
with All_Table as (
select model, price from PC
union
select model, price from Laptop
union
select model, price from Printer)

select model 
from All_Table
where price = all(select max(price) from All_Table)

--25 задача
select distinct maker 
from Product
where type = 'Printer' 
and maker 
in (select pt.maker 
from Product pt 
right join PC pc on pt.model = pc.model
where ram = (select min(ram) from PC)
 and speed = (select max(speed) 
              from PC 
              where ram = (select min(ram) from PC)))

-- 26 задача
with PC_and_Laptop as (
select pc.price, pt.maker
from Product pt
join PC pc on pt.model = pc.model
where pt.maker = 'A'
union all
select l.price, pt.maker
from Product pt
join Laptop l on pt.model = l.model
where pt.maker = 'A'
)

select avg(price)
from PC_and_Laptop

-- 27 задача
select pt.maker, avg(pc.hd)
from Product pt
right join PC pc on pt.model=pc.model
where pt.maker in(select distinct maker from Product where type = 'Printer')
group by pt.maker

-- 28 задача
select count(maker)
from Product 
where maker in (select maker from Product group by maker having count(model) = 1)

-- 29 задача
select oo.point, oo.date, io.inc, oo.out
from Outcome_o oo
left join Income_o io on oo.point = io.point and oo.date = io.date
union
select io.point, io.date, io.inc, oo.out
from Outcome_o oo
right join Income_o io on oo.point = io.point and oo.date = io.date

-- 31 задача
select class, country
from Classes
where bore >= 16

-- 33 задача 
select ship
from Outcomes
where result = 'sunk' and battle = 'North Atlantic'

-- 34 задача
select distinct sh.name
from Classes cl
full join Ships sh on cl.class = sh.class
where cl.displacement > 35000 and sh.launched >= 1922 and cl.type = 'bb' and sh.launched is not Null

-- 35 задача
select model, type
from Product
where (model like '%[^A-Z]%' and model not like '%[^0-9]%')
 or (model not like '%[^A-Z]%' and model like '%[^0-9]%')

-- 36 задача
select cl.class 
from Classes cl
join Ships sh on sh.name = cl.class
union
select cl.class 
from Classes cl
join Outcomes ou on ou.ship = cl.class

--38 задача
select country
from Classes
where type = 'bb'
INTERSECT 
select country
from Classes
where type = 'bc'

-- 39 задача
with ship_battle_damage as (
Select ou.ship as ship, bt.date as battle_date
from Outcomes ou
join Battles bt on ou.battle = bt.name
where ou.result = 'damaged')

select distinct ship 
from ship_battle_damage
where battle_date < any(select battle_dateA from (Select ou.ship as shipAL, bt.date as battle_dateA
from Outcomes ou
join Battles bt on ou.battle = bt.name) r where shipAL = ship)

-- 40 задача
select distinct maker, type
from product
where maker in (
select maker
from(
select distinct maker, type
from Product
where maker in (
Select maker
from Product
group by maker
having count(model) > 1
)) t
group by maker
having count(type) = 1)

--42 задача
Select ship, battle
from Outcomes 
where result = 'sunk'

--44 задача
Select name 
from Ships
where name like 'R%'
union 
Select ship
from Outcomes 
where ship like 'R%'

-- 45 задача
Select name 
from Ships
where name like '% % %'
union 
Select ship
from Outcomes 
where ship like '% % %'


